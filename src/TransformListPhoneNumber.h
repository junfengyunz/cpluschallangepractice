//
// Created by ethan on 12/12/18.
//

#ifndef CPLUS17CHALLANGE_TRANSFORMLISTPHONENUMBER_H
#define CPLUS17CHALLANGE_TRANSFORMLISTPHONENUMBER_H

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;
class TransformListPhoneNumber {
public:
    static void normalize_phone_numbers(vector<string>& numbers,string const & countryCode)
    {
        transform(numbers.begin(),numbers.end(),numbers.begin(),[countryCode](auto& num){
            string res;
            if(num.size()>0) {
                if (num[0] == '0')
                    res = "+" + countryCode + num.substr(1);
                else if(starts_with(num,countryCode))
                    res = "+" + num;
                else if(starts_with(num,"+"+countryCode))
                    res = num;
                else
                    res = "+" + countryCode + num;
            }

            res.erase(remove_if(res.begin(),res.end(),[](auto ch){
                return isspace(ch);

            }),res.end());

            return res;
        });

    }
    static void test()
    {
        std::vector<std::string> numbers{
                "07555 123456",
                "07555123456",
                "+44 7555 123456",
                "44 7555 123456",
                "7555 123456"
        };

        normalize_phone_numbers(numbers, "44");

        for (auto const & number : numbers)
        {
            std::cout << number << std::endl;
        }
    }
private:

    static bool starts_with(string_view str, string_view prefix)
    {
        return str.find(prefix)==0;
    }

};


#endif //CPLUS17CHALLANGE_TRANSFORMLISTPHONENUMBER_H
