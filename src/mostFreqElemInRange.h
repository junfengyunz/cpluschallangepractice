//
// Created by ethan on 12/12/18.
//

#ifndef CPLUS17CHALLANGE_MOSTFREQELEMINRANGE_H
#define CPLUS17CHALLANGE_MOSTFREQELEMINRANGE_H
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <iostream>

using namespace std;

class mostFreqElemInRange {
public:
    template<typename T>
    static vector<pair<T,size_t >> find_most_frequent(const vector<T>& range)
    {
        map<T,size_t> freq;
        for(auto&& t:range)
        {
            freq[t]++;

        }

        auto maxelem = max_element(freq.begin(),freq.end(),[](auto const & e1, auto const & e2)
        {
            return e1.second < e2.second;
        });

        vector<pair<T,size_t>> result;
        copy_if(freq.begin(),freq.end(),back_inserter(result),[maxelem](auto const & kvp)
        {
            return kvp.second == maxelem->second;
        });
        return result;

    }


    static void test()
    {
        vector<int> range = vector<int>{1,1,3,5,8,13,3,5,8,8,5};
        auto result = find_most_frequent(range);
        for(auto && e: result)
            cout << e.first << ":" << e.second << endl;
    }
};



#endif //CPLUS17CHALLANGE_MOSTFREQELEMINRANGE_H
