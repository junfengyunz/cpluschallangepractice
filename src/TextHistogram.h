//
// Created by ethan on 12/12/18.
//

#ifndef CPLUS17CHALLANGE_TEXTHISTOGRAM_H
#define CPLUS17CHALLANGE_TEXTHISTOGRAM_H
#include <string>
#include <algorithm>
#include <map>
#include <iostream>
#include <string_view>
#include <numeric>
#include <iomanip>

using namespace std;
class TextHistogram {

public:
    static map<char,double> analyze_text(string_view text)
    {
        map<char,double> charfreq;
        for(char ch='a';ch<='z'; ch++)
            charfreq[ch]=0;

        for(auto ch : text)
        {
            if(isalpha(ch))
                charfreq[tolower(ch)]++;
        }

        auto total = accumulate(charfreq.begin(),charfreq.end(),0,[](auto&sum, auto& kvp){
                return sum+ static_cast<unsigned long long>(kvp.second);
        });

        for_each(charfreq.begin(),charfreq.end(),[total](auto && kvp){
            kvp.second = kvp.second/ total;
        });
        return charfreq;
    }


    static void test()
    {

        auto result = analyze_text(R"(Lorem ipsum dolor sit amet, consectetur
adipiscing elit, sed do eiusmod tempor incididunt ut labore et
dolore magna aliqua.)");
        for(auto && kvp : result)
        {
            cout << kvp.first << ":"
            << fixed
            << setw(5) << setfill(' ')
            << setprecision(2) << kvp.second << endl;
        }
    }
};


#endif //CPLUS17CHALLANGE_TEXTHISTOGRAM_H
