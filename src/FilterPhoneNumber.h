//
// Created by ethan on 12/12/18.
//

#ifndef CPLUS17CHALLANGE_FILTERPHONENUMBER_H
#define CPLUS17CHALLANGE_FILTERPHONENUMBER_H
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <string_view>
using namespace std;
class FilterPhoneNumber {
public:

    template<typename InputIt>
    static vector<string> filter_numbers(InputIt begin, InputIt end, const string& countrycode)
    {
        vector<string> res;
        copy_if(begin,end,back_inserter(res),[countrycode](auto& number){

            return (start_with(number,countrycode)||start_with(number,'+'+countrycode));

        });
        return res;
    }

    static void test()
    {
        std::vector<std::string> numbers{
                "+40744909080",
                "44 7520 112233",
                "+44 7555 123456",
                "40 7200 123456",
                "7555 123456"
        };

        auto result = filter_numbers(numbers.begin(),numbers.end(), "44");
        for (auto & number : result)
        {
            std::cout << number << std::endl;
        }

    }
private:
    static bool start_with(string_view str, string_view prefix)
    {

        return str.find(prefix)==0;
    }

};


#endif //CPLUS17CHALLANGE_FILTERPHONENUMBER_H
